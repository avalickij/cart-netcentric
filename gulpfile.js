'use strict';
var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var newer = require('gulp-newer');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

var sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
};

gulp.task('serve', ['sass', 'scripts', 'images', 'html'], function() {

  browserSync.init({
    files: ["css/*.css", "js/*.js"],
    server: "dist"
  });

  gulp.watch("src/styles/**/*", ['sass']);
  gulp.watch('src/js/*.js', ['scripts']);
  gulp.watch('src/images/**/*', ['images']);
  gulp.watch('src/index.html', ['html']);
});

gulp.task('html', function() {
    return gulp.src('src/*.html')
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
  // Filters IE specific JS that will be loaded via conditional comments
  return gulp.src('src/js/*.js')
      .pipe(newer('dist/js', '.js'))
      .pipe(concat('main.js'))
      .pipe(gulp.dest('dist/js'))
      .pipe(rename({
        suffix: '.min'
      }))
      .pipe(uglify())
      .pipe(gulp.dest('dist/js'))
      .pipe(browserSync.stream());
});


gulp.task('sass', function () {
  return gulp
      .src("src/styles/**")
      .pipe(sourcemaps.init())
      .pipe(sass(sassOptions).on('error', sass.logError))
      .pipe(sourcemaps.write())
      .pipe(autoprefixer())
      .pipe(gulp.dest("dist/css"))
      .pipe(browserSync.stream());
});
gulp.task('images', function() {
  return gulp.src('src/images/*')
      .pipe(newer('dist/img'))
      .pipe(imagemin({
        progressive: true,
        optimizationLevel: 5,
        interlaced: true
      }))
      .pipe(gulp.dest('dist/images'))
      .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);



