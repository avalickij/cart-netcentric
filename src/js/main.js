
//A primitive, unfinished, buggy implementation of an MVVP pattern.
(function (){
    var av = {};
    window['av'] = av;
    av.BINDING_TYPES={
        'FOREACH': 'foreach',
        'TEXT': 'text',
        'NUMBER': 'number',
        'CLICK': 'click'
    };
    av.dataAttributeName = 'data-bind';
    av.utils ={
        parseBindingText: function(binding){
            return binding.split(':');
        },
        routeBinding: function($node, context, index){
            var binding = av.utils.parseBindingText($node.attr(av.dataAttributeName));
            var bindingType = binding[0];
            switch (bindingType){
                case av.BINDING_TYPES.FOREACH:
                    av.utils.foreachBinding($node, binding);
                    break;
                case av.BINDING_TYPES.TEXT:
                    av.utils.textBinding($node, binding, context, index);
                    break;
                case av.BINDING_TYPES.NUMBER:
                    av.utils.numberBinding($node, binding, context, index);
                    break;
                case av.BINDING_TYPES.CLICK:
                    av.utils.clickBinding($node, binding);
                    break;
            }
        },
        searchAndApplyBindings: function($startNode, context, index){
            $($startNode).find('*['+av.dataAttributeName+']').each(function(){
                av.utils.routeBinding($(this), context, index);
            });

        },
        foreachBinding: function($node, binding){
            //clone child nodes
            var $placeholder = $node.children();
            var context = av.viewModel[binding[1]];
            for (var i = 0; i < context.length; i++) {
                var itemToAdd = $placeholder.clone();
                $node.append(itemToAdd);
                av.utils.searchAndApplyBindings($node, context,i);
            }
            $placeholder.remove();//remove the placeholder
        },
        textBinding: function($node, binding, context, index){
            var ctx = av.viewModel[binding[1]] || context;
            if(!ctx) return;
            var bindingProperty = binding[1];
            var text = (context) ? ctx[index][bindingProperty] : ctx[bindingProperty];
            if (!$node.data('bound') && text){
                $node.text(text);
                $node.data('bound', true);
            }
        },
        numberBinding: function($node, binding, context, index){
            var ctx = av.viewModel[binding[1]] || context;
            if(!ctx) return;
            var bindingProperty = binding[1];
            var number = (context) ? ctx[index][bindingProperty] : ctx[bindingProperty];
            if (!$node.data('bound') && number){
                $node.val(number);
                $node.data('bound', true);
            }
        },
        clickBinding: function($node, binding){
            var callback = av.viewModel[binding[1]];
            if (!$node.data('bound')){
                $node.click(callback);
                $node.data('bound', true);
            }
        }


    };
    av.observable = function(variable){
        //TODO add an actual implementation. Using a dummy for now.
        return variable;
    };
    av.applyBindings = function(ViewModel){
        av.viewModel = ViewModel;
        av.utils.searchAndApplyBindings($('body'));
    }
})();


//kick off the app
(function() {
    alert('' +
        'Hi! This is an ***unfinished attempt*** to complete the assignment. ' +
        'The only dynamic thing that works at this point is listing the products ' +
        'from a JSON object -reactivity and interaction is not implemented yet. ' +
        'Due to lack of time and experience I cannot devote ' +
        'more time to finishing the assignment the way I intended. I understand ' +
        'if my application is dismissed only because of that.  ');

    alert('Due to the limitation of only using jquery, I figured ' +
        'to implement a Knockout style minimal MVVP framework, which proved ' +
        'to be a bit more involved than expected. Therefore, the app lacks ' +
        'interactivity. Ideally I would use Knockout, React or a similar library to solve the assignment. ' +
        'Feel free to explore the source and see how I approach writing' +
        'CSS, HTML and JS. Thank you for your time!');
    
    function ViewModel(){
        this.submitCart = function(){
            alert('Ajax call with jQuery');
            //var cartItems = this.cartItems;
            //simulate an ajax call. Since the
            // $.ajax({
            //     type: 'POST',
            //     url: '/api/buy,
            //     data: cartItems,
            //     success: function(){console.log('success')}
            // });
        };
        this.removeCartItem = function(){
            alert('removing cart item')
        };
        this.cartItems= av.observable([
            {
                name: 'Cotton T-Shirt, Medium',
                price:'1.99',
                quantity: 0
            },
            {
                name: 'Baseball Cap, One Size',
                price:'2.99',
                quantity: 0
            },
            {
                name: 'Baseball Cap, One Size',
                price:'3.99',
                quantity: 0
            }

        ]);
    }
    av.applyBindings(new ViewModel());
}());



